#!/usr/bin/env bash

if [[ $1 == '-c' ]]; then
    rm -rf build
fi
cmake -B build                          \
      -D CMAKE_BUILD_TYPE=Debug         \
      -D CMAKE_PREFIX_PATH=/opt/libint  \
      -D TiledArray_DIR=/opt/tiledarray \
      --verbose                         \
      -H.

cmake --build build -j7
