#include <iostream>
#include <tiledarray.h>
#include "hartree-fock.h"
#include "mp2.h"

int main(int argc, char *argv[])
{

  using std::cout;
  using std::cerr;
  using std::endl;

  using libint2::Shell;
  using libint2::Engine;
  using libint2::Operator;

  try {

    /*** =========================== ***/
    /*** initialize molecule         ***/
    /*** =========================== ***/

    // read geometry from a file; by default read from h2o.xyz, else take filename (.xyz) from the command line
    const auto filename = (argc > 1) ? argv[1] : "h2o.xyz";
    std::vector<Atom> atoms = read_geometry(filename);

    // count the number of electrons
    size_t nelectron = 0;
    for (auto i = 0; i < atoms.size(); ++i)
      nelectron += atoms[i].atomic_number;
    const size_t ndocc = nelectron / 2;

    // compute the nuclear repulsion energy
    auto enuc = 0.0;
    for (auto i = 0; i < atoms.size(); i++)
      for (auto j = i + 1; j < atoms.size(); j++) {
        auto xij = atoms[i].x - atoms[j].x;
        auto yij = atoms[i].y - atoms[j].y;
        auto zij = atoms[i].z - atoms[j].z;
        auto r2 = xij*xij + yij*yij + zij*zij;
        auto r = sqrt(r2);
        enuc += atoms[i].atomic_number * atoms[j].atomic_number / r;
      }
    cout << "\tNuclear repulsion energy = " << enuc << endl;

    /*** =========================== ***/
    /*** create basis set            ***/
    /*** =========================== ***/

    auto shells = make_sto3g_basis(atoms);
    size_t nao = 0;
    for (auto s=0; s<shells.size(); ++s)
      nao += shells[s].size();
    size_t nvirt = nao - ndocc;

    /*** =========================== ***/
    /*** compute 1-e integrals       ***/
    /*** =========================== ***/

    // initializes the Libint integrals library ... now ready to compute
    libint2::initialize();

    // compute overlap integrals
    auto S = compute_1body_ints(shells, Operator::overlap);
    cout << "\n\tOverlap Integrals:\n";
    cout << S << endl;

    // compute kinetic-energy integrals
    auto T = compute_1body_ints(shells, Operator::kinetic);
    cout << "\n\tKinetic-Energy Integrals:\n";
    cout << T << endl;

    // compute nuclear-attraction integrals
    Matrix V = compute_1body_ints(shells, Operator::nuclear, atoms);
    cout << "\n\tNuclear Attraction Integrals:\n";
    cout << V << endl;

    // Core Hamiltonian = T + V
    Matrix H = T + V;
    cout << "\n\tCore Hamiltonian:\n";
    cout << H << endl;

    // T and V no longer needed, free up the memory
    T.resize(0,0);
    V.resize(0,0);

    /*** =========================== ***/
    /*** build initial-guess density ***/
    /*** =========================== ***/

    // use core Hamiltonian eigenstates to guess density?
    // set to true to match the result of versions 0, 1, and 2 of the code
    // HOWEVER !!! even for medium-size molecules hcore will usually fail !!!
    // thus set to false to use Superposition-Of-Atomic-Densities (SOAD) guess
    const auto use_hcore_guess = false; 

    Matrix D;
    Matrix C;
    Eigen::VectorXd C_v; // eigenvalues
    if (use_hcore_guess) { // hcore guess
      // solve H C = e S C
      Eigen::GeneralizedSelfAdjointEigenSolver<Matrix> gen_eig_solver(H, S);
      auto eps = gen_eig_solver.eigenvalues();
      auto C = gen_eig_solver.eigenvectors();
      cout << "\n\tInitial C Matrix:\n";
      cout << C << endl;

      // compute density, D = C(occ) . C(occ)T
      auto C_occ = C.leftCols(ndocc);
      D = C_occ * C_occ.transpose();
    }
    else {  // SOAD as the guess density, assumes STO-nG basis
      D = compute_soad(atoms);
    }

    cout << "\n\tInitial Density Matrix:\n";
    cout << D << endl;

    /*** =========================== ***/
    /*** main iterative loop         ***/
    /*** =========================== ***/

    const auto maxiter = 100;
    const auto conv = 1e-12;
    auto iter = 0;
    auto rmsd = 0.0;
    auto ediff = 0.0;
    auto ehf = 0.0;
    Matrix fock_mat; // capture fock matrix for ccsd calculations
    do {
      const auto tstart = std::chrono::high_resolution_clock::now();
      ++iter;

      // Save a copy of the energy and the density
      auto ehf_last = ehf;
      auto D_last = D;

      // build a new Fock matrix
      auto F = H;
      //F += compute_2body_fock_simple(shells, D);
      F += compute_2body_fock(shells, D);
      fock_mat = F;

      if (iter == 1) {
        cout << "\n\tFock Matrix:\n";
        cout << F << endl;
      }

      // solve F C = e S C
      Eigen::GeneralizedSelfAdjointEigenSolver<Matrix> gen_eig_solver(F, S);
      auto eps = gen_eig_solver.eigenvalues();
      C = gen_eig_solver.eigenvectors();
      C_v = gen_eig_solver.eigenvalues();

      // compute density, D = C(occ) . C(occ)T
      auto C_occ = C.leftCols(ndocc);
      D = C_occ * C_occ.transpose();

      // compute HF energy
      ehf = 0.0;
      for (auto i = 0; i < nao; i++)
        for (auto j = 0; j < nao; j++)
          ehf += D(i,j) * (H(i,j) + F(i,j));

      // compute difference with last iteration
      ediff = ehf - ehf_last;
      rmsd = (D - D_last).norm();

      const auto tstop = std::chrono::high_resolution_clock::now();
      const std::chrono::duration<double> time_elapsed = tstop - tstart;

      if (iter == 1)
        cout <<
        "\n\n Iter        E(elec)              E(tot)               Delta(E)             RMS(D)         Time(s)\n";
      printf(" %02d %20.12f %20.12f %20.12f %20.12f %10.5lf\n", iter, ehf, ehf + enuc,
             ediff, rmsd, time_elapsed.count());

    } while (((fabs(ediff) > conv) || (fabs(rmsd) > conv)) && (iter < maxiter));

    libint2::finalize(); // done with libint
     
    auto hf_final = ehf + enuc;
    printf("** Hartree-Fock energy = %20.12f\n", hf_final);

    /*** =========================== ***/
    /***          MP2 energy         ***/
    /*** =========================== ***/
    //

    /* cout << "Coeff matrix is: " << endl; */
    /* cout << C << endl << endl; */

    // Initialize MADWorld
    TA::World& world = TA::initialize(argc, argv);

    cout << endl;
    cout << "*********************************" << endl;
    cout << "Calculating EMP2 using TiledArray" << endl;
    cout << "*********************************" << endl;

    auto mo_ints_tensor     = compute_mo_ints(C, shells, world);
    auto ints_tile_spatial  = mo_ints_tensor.find({0,0,0,0}).get();

    // from here on number of occupied and virtual orbitals
    // is doubled as we are now working on spin basis
    nao         *= 2;
    nvirt       *= 2;
    size_t nocc  = 2*ndocc; // in place of ndocc use nocc

    auto spinify_ints = [&](const TA::Range& range) {
      TA::Tensor<double> tile(range);
      for (auto r = 0; r < nao; ++r) {
        for (auto s = 0; s < nao; ++s) {
          for (auto p = 0; p < nao; ++p) {
            for (auto q = 0; q < nao; ++q) {
              //
              // finding the spatial orbital position
              // corresponding to a spin orbital
              //
              // spin    spatial
              // ---------------
              // 0,1        -> 0
              // 2,3        -> 1
              // 4,5        -> 2
              // 2*n, 2*n+1 -> n
              // x          -> floor(x/2)
              //
              size_t p_i = floor(p/2);
              size_t q_i = floor(q/2);
              size_t r_i = floor(r/2);
              size_t s_i = floor(s/2);

              auto col_int = 0.0;
              auto exc_int = 0.0;

              // orbs: 0 1 2 3 4 ...
              // spin: α β α β α ...
              // 
              //  <01|23>: coulomb integral

              if ((r%2 == p%2) && (s%2 == q%2))
                col_int  += ints_tile_spatial(r_i, s_i, p_i, q_i);

              if ((r%2 == q%2) && (s%2 == p%2))
                exc_int  += ints_tile_spatial(r_i, s_i, q_i, p_i);

              tile(r,s,p,q) = col_int - exc_int;
            }
          }
        }
      }
      return tile;
    };

    TA::TArrayD mo_ints_tensor_spin(world,
                                    TA::TiledRange{{0, nao}, {0, nao},
                                    {0, nao}, {0, nao}});

    auto tile_spin_ints = mo_ints_tensor_spin.world().taskq.add(spinify_ints,
                mo_ints_tensor_spin.trange().make_tile_range(0));
    *(mo_ints_tensor_spin.begin()) = tile_spin_ints;

    auto spinify_fock = [&](const TA::Range& range){
      TA::Tensor<double> tile(range);
      for (auto i = 0; i < nao; ++i){
        for (auto j = 0; j < nao; ++j)
          tile(i,j) = (i == j)? C_v(floor(i/2)): 0;
      }
      return tile;
    };

    TA::TArrayD fock_spin(world, TA::TiledRange{{0, nao}, {0, nao}});
    auto tile_fock_spin = fock_spin.world().taskq.add(spinify_fock,
                           fock_spin.trange().make_tile_range(0));
    *(fock_spin.begin()) = tile_fock_spin;

    // See Step 4 at
    // http://sirius.chem.vt.edu/wiki/doku.php?id=crawdad:programming:project4
    //

    auto ints_tile_spin = mo_ints_tensor_spin.find({0,0,0,0}).get();
    auto fock_tile = fock_spin.find({0,0}).get();
    auto emp2 = 0.0;

    for (auto r = 0; r < nocc; ++r) {
        for (auto s = 0; s < nocc; ++s) {
            for (auto p = nocc; p < nao; ++p) {
                for (auto q = nocc; q < nao; ++q) {

                  auto calc = ints_tile_spin(r,s,p,q);

                  calc *= calc;

                  emp2 += calc/(fock_tile(r,r)
                      + fock_tile(s,s)
                      - fock_tile(p,p)
                      - fock_tile(q,q));
                }
            }
        }
    }
    emp2 /= 4.0;
    cout << "\nThe MP2 correlation energy: " << emp2 << endl;
    cout << "Final energy is : " << hf_final + emp2 << endl;
    TA::finalize();
  } // end of try block; if any exceptions occurred, report them and exit cleanly

  catch (const char* ex) {
    cerr << "caught exception: " << ex << endl;
    return 1;
  }
  catch (std::string& ex) {
    cerr << "caught exception: " << ex << endl;
    return 1;
  }
  catch (std::exception& ex) {
    cerr << ex.what() << endl;
    return 1;
  }
  catch (...) {
    cerr << "caught unknown exception\n";
    return 1;
  }

  return 0;
}
