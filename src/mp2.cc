/******************************
 * MP2 Primer                 *
 * An extension to HF Primer  *
 *                            *
 ******************************/
#include <cstddef>
#include <iostream>
#include <algorithm>
#include <vector>

#include <tiledarray.h>

#include "hartree-fock.h"
#include "mp2.h"

TA::TArrayD compute_mo_ints(const Matrix& coff_mat,
                     const std::vector<libint2::Shell>& shells,
                     madness::World& world) {
  // returns 2e integrals on MO basis in physicist's notation

  using libint2::Shell;
  using libint2::Engine;
  using libint2::Operator;

  const auto nao = nbasis(shells);

  // *************************************
  // first calculate integrals on AO basis
  // *************************************

  auto fill_eri_tensor = [&](const TA::Range& range){

    // allocate the tensor to return
    TA::Tensor<double> tile(range);

    libint2::initialize();
    // construct the electron repulsion integrals engine
    Engine engine(Operator::coulomb, max_nprim(shells), max_l(shells), 0);

    auto shell2bf = map_shell_to_basis_function(shells);

    // buf[0] points to the target shell set after every call  to engine.compute()
    const auto& buf = engine.results();

    // loop over shell pairs of the Fock matrix, {s1,s2}
    // Fock matrix is symmetric, but skipping it here for simplicity (see compute_2body_fock)
    for(auto s1=0; s1!=shells.size(); ++s1) {

      auto bf1_first = shell2bf[s1]; // first basis function in this shell
      auto n1 = shells[s1].size();

      for(auto s2=0; s2!=shells.size(); ++s2) {

        auto bf2_first = shell2bf[s2];
        auto n2 = shells[s2].size();

        // loop over shell pairs of the density matrix, {s3,s4}
        // again symmetry is not used for simplicity
        for(auto s3=0; s3!=shells.size(); ++s3) {

          auto bf3_first = shell2bf[s3];
          auto n3 = shells[s3].size();

          for(auto s4=0; s4!=shells.size(); ++s4) {

            auto bf4_first = shell2bf[s4];
            auto n4 = shells[s4].size();

            // Coulomb contribution to the Fock matrix is from {s1,s2,s3,s4} integrals
            engine.compute(shells[s1], shells[s2], shells[s3], shells[s4]);
            const auto* buf_1234 = buf[0];
            if (buf_1234 == nullptr)
              continue; // if all integrals screened out, skip to next quartet

            // we don't have an analog of Eigen for tensors (yet ... see github.com/BTAS/BTAS, under development)
            // hence some manual labor here:
            // 1) loop over every integral in the shell set (= nested loops over basis functions in each shell)
            // and 2) add contribution from each integral
            for(auto f1=0, f1234=0; f1!=n1; ++f1) {
              const auto bf1 = f1 + bf1_first;
              for(auto f2=0; f2!=n2; ++f2) {
                const auto bf2 = f2 + bf2_first;
                for(auto f3=0; f3!=n3; ++f3) {
                  const auto bf3 = f3 + bf3_first;
                  for(auto f4=0; f4!=n4; ++f4, ++f1234) {
                    const auto bf4 = f4 + bf4_first;
                    tile(bf1, bf2, bf3, bf4) = buf_1234[f1234];
                  }
                }
              }
            }
          }
        }
      }
    } // computed ints on AO basis
    libint2::finalize(); // done with libint
    return tile;
  };

  // create ints_ao
  TA::TiledRange trange_ao{{0, nao}, {0, nao}, {0, nao}, {0, nao}};
  TA::TArrayD    ints_ao(world, trange_ao);
  auto tile = ints_ao.world().taskq.add(fill_eri_tensor,
                                ints_ao.trange().make_tile_range(0));
  *(ints_ao.begin()) = tile;

  // *********************
  // transform to MO basis
  // *********************
  //

  auto mat2tensor = [&](const TA::Range& range){
    TA::Tensor<double> tile(range);
    std::copy(coff_mat.data(),
        coff_mat.data()+coff_mat.size(),
        tile.begin());
    return tile;
  };
  // transform the coefficient Eigen::MatrixD to TA::Tensor<double> type
  TA::TArrayD coeff_tensor(world, TA::TiledRange{{0,nao},{0,nao}});
  tile = coeff_tensor.world().taskq.add(mat2tensor,
                                      coeff_tensor.trange().make_tile_range(0));
  *(coeff_tensor.begin()) = tile;

  TA::TArrayD ints_mo(world, trange_ao);
  ints_mo("5,1,2,3") = ints_ao("1,2,3,4") * coeff_tensor("4,5"); // s contract
  ints_mo("5,1,2,3") = ints_mo("1,2,3,4") * coeff_tensor("4,5"); // rs contract
  ints_mo("5,1,2,3") = ints_mo("1,2,3,4") * coeff_tensor("4,5"); // qrs contract
  ints_mo("5,1,2,3") = ints_mo("1,2,3,4") * coeff_tensor("4,5"); // pqrs contract

  // into Physicist's notation
  ints_mo("1,2,3,4") = ints_mo("1,3,2,4");
  return ints_mo;
}
